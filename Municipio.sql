create table Municipio (
	idMunicipio INT,
	nombre VARCHAR(50),
	idEstado INT
);
insert into Municipio (idMunicipio, nombre, idEstado) values (1, 'Zambujal', 1);
insert into Municipio (idMunicipio, nombre, idEstado) values (2, 'Tower', 2);
insert into Municipio (idMunicipio, nombre, idEstado) values (3, 'Golubinci', 3);
insert into Municipio (idMunicipio, nombre, idEstado) values (4, 'Rakek', 4);
insert into Municipio (idMunicipio, nombre, idEstado) values (5, 'Ghāro', 5);
insert into Municipio (idMunicipio, nombre, idEstado) values (6, 'Koźmin Wielkopolski', 6);
insert into Municipio (idMunicipio, nombre, idEstado) values (7, 'Ust’-Labinsk', 7);
insert into Municipio (idMunicipio, nombre, idEstado) values (8, 'Jasień', 8);
insert into Municipio (idMunicipio, nombre, idEstado) values (9, 'Mandiana', 9);
insert into Municipio (idMunicipio, nombre, idEstado) values (10, 'Härnösand', 10);
