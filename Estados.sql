create table Estados (
	idEstado INT,
	nombre VARCHAR(50)
);
insert into Estados (idEstado, nombre) values (1, 'Gorelovo');
insert into Estados (idEstado, nombre) values (2, 'San Antonio');
insert into Estados (idEstado, nombre) values (3, 'Boynton Beach');
insert into Estados (idEstado, nombre) values (4, 'Hanyuan');
insert into Estados (idEstado, nombre) values (5, 'Skhodnya');
insert into Estados (idEstado, nombre) values (6, 'Luau');
insert into Estados (idEstado, nombre) values (7, 'Bueng Sam Phan');
insert into Estados (idEstado, nombre) values (8, 'Tampa');
insert into Estados (idEstado, nombre) values (9, 'Tawau');
insert into Estados (idEstado, nombre) values (10, 'Rawa Satu');
