create table Personas (
	id_RegCivil INT,
	Nombre VARCHAR(50),
	Telefono INT,
	email VARCHAR(50),
	id_Juez INT
);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (1, 'Aarika', 49, 'apolgreen0@google.co.uk', 1);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (2, 'Obie', 14, 'ofeek1@themeforest.net', 2);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (3, 'Mirabelle', 68, 'mheinonen2@free.fr', 3);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (4, 'Dorris', 60, 'dhaldenby3@pcworld.com', 4);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (5, 'Marysa', 81, 'mtrenaman4@de.vu', 5);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (6, 'Holmes', 7, 'hreek5@examiner.com', 6);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (7, 'Agretha', 8, 'amaccracken6@bloglines.com', 7);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (8, 'Anabel', 47, 'aglasheen7@mashable.com', 8);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (9, 'Mikel', 6, 'mellum8@ibm.com', 9);
insert into Personas (id_RegCivil, Nombre, Telefono, email, id_Juez) values (10, 'Sheridan', 22, 'shadrill9@who.int', 10);
