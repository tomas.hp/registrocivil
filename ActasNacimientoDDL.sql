CREATE TABLE `Acta_Personas` (
	`idPersona` INT(2) NOT NULL AUTO_INCREMENT,
	`Nombres` TEXT(64) NOT NULL,
	`apPaterno` TEXT(64) NOT NULL,
	`apMaterno` TEXT(64) NOT NULL,
	`curp` VARCHAR(50) NOT NULL,
	`rfc` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`idPersona`)
);

CREATE TABLE `actaNacimiento` (
	`idActa` INT(2) NOT NULL AUTO_INCREMENT,
	`idRegistrado` INT(2) NOT NULL,
	`idPadre` INT(2),
	`idMama` INT(2),
	`idAbuelo` INT(2) NOT NULL,
	`idEstado` INT(2) NOT NULL,
	`idRegistro` BINARY(2) NOT NULL,
	`fechaNacimiento` DATE NOT NULL,
	PRIMARY KEY (`idActa`)
);

CREATE TABLE `Municipio` (
	`idMunicipio` INT(2) NOT NULL AUTO_INCREMENT,
	`Nombre` VARCHAR(30) NOT NULL,
	`idEstado` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`idMunicipio`)
);

CREATE TABLE `Estados` (
	`idEstado` INT(2) NOT NULL,
	`Nombre` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`idEstado`)
);

CREATE TABLE `RegistroCivil` (
	`idRegistroCivil` BINARY(2) NOT NULL,
	`NombreRegistro` varchar(30) NOT NULL,
	`idJuez` INT(2) NOT NULL,
	`Nombre` VARCHAR(60) NOT NULL,
	`Telefono` INT(10) NOT NULL,
	`id_Juez` INT(2) NOT NULL,
	PRIMARY KEY (`idRegistroCivil`)
);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk0` FOREIGN KEY (`idRegistrado`) REFERENCES `Acta_Personas`(`idPersona`);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk1` FOREIGN KEY (`idPadre`) REFERENCES `Acta_Personas`(`idPersona`);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk2` FOREIGN KEY (`idMama`) REFERENCES `Acta_Personas`(`idPersona`);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk3` FOREIGN KEY (`idAbuelo`) REFERENCES `Acta_Personas`(`idPersona`);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk4` FOREIGN KEY (`idEstado`) REFERENCES `Estados`(`idEstado`);

ALTER TABLE `actaNacimiento` ADD CONSTRAINT `actaNacimiento_fk5` FOREIGN KEY (`idRegistro`) REFERENCES `RegistroCivil`(`idRegistroCivil`);

ALTER TABLE `Municipio` ADD CONSTRAINT `Municipio_fk0` FOREIGN KEY (`idEstado`) REFERENCES `Estados`(`idEstado`);

ALTER TABLE `RegistroCivil` ADD CONSTRAINT `RegistroCivil_fk0` FOREIGN KEY (`idJuez`) REFERENCES `Acta_Personas`(`idPersona`);

