create table actaNacimiento (
	idActa INT,
	idRegistrado INT,
	idPadre INT,
	idMama INT,
	idAbuelo INT,
	idEstado INT,
	idRegistro INT
);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (1, 1, 1, 1, 1, 1, 1);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (2, 2, 2, 2, 2, 2, 2);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (3, 3, 3, 3, 3, 3, 3);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (4, 4, 4, 4, 4, 4, 4);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (5, 5, 5, 5, 5, 5, 5);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (6, 6, 6, 6, 6, 6, 6);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (7, 7, 7, 7, 7, 7, 7);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (8, 8, 8, 8, 8, 8, 8);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (9, 9, 9, 9, 9, 9, 9);
insert into actaNacimiento (idActa, idRegistrado, idPadre, idMama, idAbuelo, idEstado, idRegistro) values (10, 10, 10, 10, 10, 10, 10);
